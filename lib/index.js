/* eslint camelcase: [0] */
import channelPlugin from './channel';
import downloadPlugin from './download';

const plugins = {
  youtube_channel: channelPlugin,
  youtube_download: downloadPlugin,
};

export {plugins};

export default {plugins};
