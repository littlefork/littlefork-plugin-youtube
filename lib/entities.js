import {merge} from 'lodash/fp';
import moment from 'moment';

export const video = item =>
    merge(item, {
      _lf_id_fields: ['id'],
      _lf_content_fields: ['snippet.title', 'snippet.description'],
      _lf_pubdates: {source: moment.utc(item.snippet.publishedAt).toDate()},
      _lf_links: [{
        type: 'self',
        term: `https://www.youtube.com/watch?v=${item.id}`,
      }, {
        type: 'thumbnail',
        term: item.snippet.thumbnails.high.url,
        width: item.snippet.thumbnails.high.width,
        height: item.snippet.thumbnails.high.height,
      }],
      _lf_media: [{
        type: 'thumbnail',
        term: item.snippet.thumbnails.high.url,
        width: item.snippet.thumbnails.high.width,
        height: item.snippet.thumbnails.high.height,
      }, {
        type: 'video',
        term: `https://www.youtube.com/watch?v=${item.id}`,
      }],
      _lf_downloads: [{
        type: 'youtube_video',
        term: `https://www.youtube.com/watch?v=${item.id}`,
        videoId: item.id,
      }],
    });

export const playlistVideo = item =>
    merge(item, {
      id: item.contentDetails.videoId,
      playlist_id: item.id,
      _lf_id_fields: ['id'],
      _lf_content_fields: ['snippet.title', 'snippet.description'],
      _lf_pubdates: {source: moment.utc(item.snippet.publishedAt).toDate()},
      _lf_links: [{
        type: 'self',
        term: `https://www.youtube.com/watch?v=${item.id}`,
      }, {
        type: 'thumbnail',
        term: item.snippet.thumbnails.high.url,
        width: item.snippet.thumbnails.high.width,
        height: item.snippet.thumbnails.high.height,
      }],
      _lf_media: [{
        type: 'thumbnail',
        term: item.snippet.thumbnails.high.url,
        width: item.snippet.thumbnails.high.width,
        height: item.snippet.thumbnails.high.height,
      }, {
        type: 'video',
        term: `https://www.youtube.com/watch?v=${item.contentDetails.videoId}`,
      }],
      _lf_downloads: [{
        type: 'youtube_video',
        term: `https://www.youtube.com/watch?v=${item.contentDetails.videoId}`,
        videoId: item.contentDetails.videoId,
      }],
    });

export default {video, playlistVideo};
